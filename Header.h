#pragma once
/**
 * @file Header.h
 * @author Gabriel Valvin (gabriel.valvin@etu.uca.fr) & Alexia Philip (alexia.philip@etu.uca.fr)
 * @brief Header du code contenant les structure et les instanciation des fonctions du programme
 * @version 0.1
 * @date 2020-10-12
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once
#ifndef __HEADER_H__
#define __HEADER_H__

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

const int nmax = 50;
const int mmax = 50;
const int infini = 9999;

/**
 * @struct T_INSTANCE
 * @brief Instance du probl�me avec le nombre de machine, le nombre de pi�ce et les matrices utiles pour le probl�me
 *
 */
typedef struct T_INSTANCE {
	int n;
	int m;
	int machine[nmax][mmax] = { 0 };
	int P[nmax][mmax] = { 0 };
	int P_prim[nmax * mmax] = { 0 };
	int M_prim[mmax * nmax] = { 0 };
}T_INSTANCE;
/**
 * @struct T_VECTEUR
 * @brief Structure contenant le vecteur de Bierwith
 *
 */
typedef struct T_VECTEUR {
	int V[(nmax) * (mmax)] = { 0 }; //vecteur de bierwith
	int St[nmax] = {0};
	int cout;
	int Pere[nmax][mmax];
	int Pred[nmax] = { 0 };
	int nbop[nmax] = { 0 };
	int N[nmax] = { 0 };
	int cMax = 0;
}T_VECTEUR;

//il faut la valeur de la signature de la fonction de hachage


//              #######################################
//				####  DECLARATION DES PROCEDURES   ####
//              #######################################

void lecture(string nom, T_INSTANCE& mon_intance);
void evaluer(T_INSTANCE& mon_instance, T_VECTEUR& mon_vecteur);
void genererVecteurBierwith(T_VECTEUR& s, T_INSTANCE& uneInstance);
void afficherVecteurBierwith(T_INSTANCE& mon_instance, T_VECTEUR &solution);
void recherche_locale(T_INSTANCE& mon_instance, T_VECTEUR& mon_vecteur, int nbmax_iteration);
#endif