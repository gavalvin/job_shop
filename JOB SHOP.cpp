/**
 * @file Job-shop.cpp
 * @author Gabriel Valvin (gabriel.valvin@etu.uca.fr) & Alexia Philip (alexia.philip@etu.uca.fr)
 * @brief Fonction main du probleme de job shop
 * @version 0.1
 * @date 2020-10-12
 *
 * @copyright Copyright (c) 2020
 *
 */
#include <iostream>
#include "Header.h"

int main(void)
{
	T_INSTANCE mon_instance;
	T_VECTEUR vecteur;

	lecture("la01.txt", mon_instance);
	cout << "Nombre de machine" << mon_instance.n << endl;
	cout << "Nombre de piece" << mon_instance.m << endl;

	cout << "Evaluation " << endl;
	evaluer(mon_instance, vecteur);
	genererVecteurBierwith(vecteur,mon_instance);
	afficherVecteurBierwith(mon_instance, vecteur);


	return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
