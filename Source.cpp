﻿/**
 * @file Source.cpp
 * @author Gabriel Valvin (gabriel.valvin@etu.uca.fr) & Alexia Philip (alexia.philip@etu.uca.fr)
 * @brief Code source du problème de job shop
 * @version 0.1
 * @date 2020-10-12
 *
 * @copyright Copyright (c) 2020
 *
 */
#include "Header.h"
#include <fstream>
#include <tuple>
#include <stdexcept>
#include <random>
#include <ctime>


/* test */
//       #### EXEMPLE DE FICHIER ####

/*instance la01

 ++++++++++++++++++++++++++++ +
 Lawrence 10x5 instance(Table 3, instance 1); also called(setf1) or (F1)
 10 5							
 1 21 0 53 4 95 3 55 2 34		
 0 21 3 52 4 16 2 26 1 71
 3 39 4 98 1 42 2 31 0 12
 1 77 0 55 4 79 2 66 3 77
 0 83 3 34 2 64 1 19 4 37
 1 54 2 43 4 79 0 92 3 62
 3 69 4 77 1 87 2 87 0 93
 2 38 0 60 1 41 3 24 4 83
 3 17 1 49 4 25 0 44 2 98
 4 77 3 79 2 43 1 75 0 96
 */

int alexia=9;

/**
  * @brief Fonction de lecture d'un fichier texte contenant un problème de job shop
  *
  * @param nom nom du fichier contenant la matrice du problème
  * @param mon_instance Instance mise en paramètre afin de rentrer les valeurs du fichier dans la structure visant à etre manipulé
  */
void lecture(string nom, T_INSTANCE &mon_instance)
{
	fstream fichier(nom, ios::in);
	int valeur;
	if (fichier)
	{
		fichier >> mon_instance.n;
		fichier >> mon_instance.m;
	}
	else
	{
		perror("lecture(string,T_INSTANCE) : fichier vide ou inexistant");
		exit(-1);
	}

	for (int i = 0; i < nmax; ++i)
	{
		for (int j = 0; j < mmax; ++j)
		{
			fichier >> valeur;
			valeur++;
			mon_instance.machine[i][j] = valeur;
			fichier >> mon_instance.P[i][j];
		}
	}
	cout << "Lecture du fichier effectue" << endl;
	fichier.close();
}

/**
 * @brief Fonction d'évaluation de la solution
 *
 * @param mon_instance instance du problème
 * @param mon_vecteur vecteur solution
 */
void evaluer(T_INSTANCE &mon_instance, T_VECTEUR &mon_vecteur)
{
	int np[nmax] = {0};
	tuple<int, int> mp[mmax] = {make_tuple(-1, -1)};
	mon_vecteur.St[0][0] = 0;

	for (int i = 1; i <= nmax + 1; i++)
	{
		for (int j = 1; j <= mmax + 1; j++)
		{
			mon_vecteur.St[i][j] = -infini;
		}
	}

	int long_vecteur = (mon_instance.n) * (mon_instance.m);

	for (int i = 0; i < long_vecteur; i++)
	{
		int j = mon_vecteur.V[i];
		np[j]++;
		int mc = mon_instance.machine[j][np[j]];
		if (np[j] > 1)
		{
			int debut_piece = mon_vecteur.St[j][np[j] - 1]; //date de debut de traitement de la piece
			int fin_piece = debut_piece + mon_instance.P[j][np[j] - 1];
			if (fin_piece > mon_vecteur.St[j][np[j]])
			{
				mon_vecteur.St[j][np[j]] = fin_piece;
			}
			if (mon_vecteur.St[j][np[j]] > mon_vecteur.cout)
			{
				mon_vecteur.cout = mon_vecteur.St[j][np[j]];
			}
		}
		tuple<int, int> a = make_tuple(-1, -1);
		if (mp[mc] != a)
		{
			int pc = get<0>(mp[mc]);
			int rc = get<1>(mp[mc]);
			if (mon_vecteur.St[pc][rc] + mon_instance.P[pc][rc] > mon_vecteur.St[j][np[j]])
			{
				mon_vecteur.St[j][np[j]] = mon_vecteur.St[pc][rc] + mon_instance.P[pc][rc];
			}
			if (mon_vecteur.St[j][np[j]] > mon_vecteur.cout)
			{
				mon_vecteur.cout = mon_vecteur.St[j][np[j]];
			}
		}
		tuple<int, int> b = make_tuple(j, np[j]);
		mp[mc] = b;
	}
}

void genererVecteurBierwith(T_VECTEUR &s, T_INSTANCE &uneInstance)
{
	int compteur[nmax] = {0};
	int valSouhaitee[nmax];
	int nbSouhaitee = uneInstance.n;
	int nbRandom;

	int longueur = uneInstance.n * uneInstance.m;

	if (s.V == NULL)
	{
		cout << "genererVecteurBierwith() : Problème d'allocation" << endl;
	}

	srand(1);

	for (int i = 0; i < uneInstance.n; i++)
	{
		valSouhaitee[i] = i + 1;
	}

	for (int i = 0; i < longueur; i++)
	{
		nbRandom = rand() % nbSouhaitee + 1;
		(compteur[nbRandom - 1])++;
		(s.V)[i] = valSouhaitee[nbRandom - 1];

		if (compteur[nbRandom - 1] >= uneInstance.m)
		{
			if (nbRandom != nbSouhaitee)
			{
				compteur[nbRandom - 1] = compteur[nbSouhaitee - 1];
				valSouhaitee[nbRandom - 1] = valSouhaitee[nbSouhaitee - 1];
			}
			--nbSouhaitee;
		}
	}
}

void initSolution(T_VECTEUR &solution, T_INSTANCE &instance)
{
	for (int k = 0; k <= instance.n + 1; ++k)
	{
		solution.St[k] = 0;
		solution.Pred[k] = 0;
		solution.nbop[k] = 0;
		solution.N[k] = 0;
	}
}

void recherche_locale(T_INSTANCE &mon_instance, T_VECTEUR &mon_vecteur, int nbmax_iteration)
{
	int iteration = 0;
	int iterationMax = 30; // cas ou on fait trop d'itération afin d'améliorer les performances"
	int i = mon_vecteur.Pred[mon_instance.n];
	int j = mon_vecteur.Pred[i];
	int temp;
	T_VECTEUR RESET;
	T_VECTEUR tempSol;

	while (j != 0 && iteration < iterationMax)
	{
		if (mon_instance.M_prim[j] == mon_instance.M_prim[i])
		{
			initSolution(tempSol, mon_instance);

			for (int k = 0; k <= mon_instance.n; ++k)
			{
				tempSol.V[k] = mon_vecteur.V[k];
			}

			temp = tempSol.V[i - 1];
			tempSol.V[i - 1] = tempSol.V[j];
			tempSol.V[j] = temp;

			evaluer(mon_instance, tempSol);

			if (mon_vecteur.cMax > tempSol.cMax)
			{
				mon_vecteur = tempSol;
				tempSol = RESET;

				for (int k = 0; k <= mon_instance.n; ++k)
				{
					tempSol.V[k] = mon_vecteur.V[k];
				}
				i = mon_vecteur.Pred[mon_instance.n];
				j = mon_vecteur.Pred[i];
			}
			else
			{
				i = j;
				j = mon_vecteur.Pred[j];
			}
		}
		else
		{
			iteration++;
		}
	}
}

void afficherVecteurBierwith(T_INSTANCE &mon_instance, T_VECTEUR &solution)
{
	std::cout << "V = (";
	int longueur;
	longueur = (mon_instance.n + 1) * (mon_instance.m + 1);
	for (int i = 0; i < (longueur - 1); i++)
	{
		if ((i % mon_instance.n) == 0)
		{
			std::cout << endl;

		}
		else
		{
			cout << (solution.V)[i] << ", ";
		}
	}

	cout << (solution.V)[(longueur - 1)] << ")";
}